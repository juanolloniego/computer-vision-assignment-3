import cv2
import numpy as np
from sklearn.svm import LinearSVC
from keras.applications import ResNet50
from get_images_paths import get_training_test_paths_labels, get_testing_paths


def compute_features(input_paths):
    """
    :param input_paths: The filesystem paths to the images
    :return: The feature vectors corresponding to each image in the input list.
    """
    result = []
    for image_path in input_paths:
        # Read the image and resize it
        image = cv2.imread(image_path, 3)
        image = cv2.resize(image, (256, 256))
        image = np.expand_dims(image, 0)
        # Get the feature vector from the neural net and flatten it
        features = resnet_model.predict(image)
        result.append(np.reshape(features, features.size))
    return result


# Load the keras model for the ResNet50 model trained on the imagenet dataset
# Exclude the top (last) layer since that does classification.
resnet_model = ResNet50(weights='imagenet', include_top=False, input_shape=(256, 256, 3))

# Load training and test data from the labeled dataset.
training_inputs, test_inputs, training_labels, test_labels = get_training_test_paths_labels()

print("Computing training features")
training_features = compute_features(training_inputs)
print("Features Done\n")

print("Training the classifier")
classifier = LinearSVC(multi_class='ovr', max_iter=20000)
classifier.fit(training_features, training_labels)
print("Classifier Done\n")

print("Computing the Test Data features")
test_features = compute_features(test_inputs)
print("Test features Done\n")

# Count the true positives in the predictions
print("Predictions for the test set of the training data")
predictions = classifier.predict(test_features)
correct = 0
for idx, label in enumerate(test_labels):
    if label == predictions[idx]:
        correct += 1

print(correct, " out of ", len(test_labels))
print(correct/len(test_labels)*100)
print("")

print("Predicting the labels for the unseen data and writing them to a file.")
# Predict the labels of the real test images and output a file result
real_test_paths = get_testing_paths()
real_test_features = compute_features(real_test_paths)
predictions = classifier.predict(real_test_features)

with open("run3.txt", 'w') as out_file:
    for i in range(0, len(predictions)):
        image_name = real_test_paths[i].split("/")[-1]
        out_file.write("{} {}\n".format(image_name, predictions[i]))

print("All Done!")
