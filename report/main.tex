\documentclass[a4paper, 11pt]{article}
\usepackage{comment} % enables the use of multi-line comments
\usepackage{fullpage} % changes the margin

\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{textcomp, gensymb}
\usepackage{cancel}
\usepackage{wrapfig}
\usepackage{float}
\usepackage[english]{babel}
\usepackage{hyperref}

%\usepackage[utf8]{inputenc}
\usepackage[colorinlistoftodos]{todonotes}

\begin{document}
%Header-Make sure you update this information
\noindent
\normalsize\textbf{Coursework 3 Report} \hfill \textbf{Sulaiman Sadiq} - Student ID: 29798655 \\
\normalsize \textbf{COMP6223} Computer Vision (MSc)\hfill \textbf{Juan Olloniego} - Student ID: 30481295 \\

\section{Run 1, K Nearest Neighbours}

\textbf{The Tiny Features:} In the first run we developed a K Nearest Neighbours (KNN) classifier based on the tiny image feature. To calculate this feature, the image is first cropped to get an odd number of pixels in both dimensions. The center point is then calculated and the image is cropped to a square around it. Subsequently, the image is resized to a 16x16 tiny image which is flattened out, mean centered and normalized.
\medskip

\noindent
\textbf{Training the Classifier:} After the tiny features have been calculated for all 1500 images in the training set, the training set is further split into a training and test set with an 80:20 ratio to work out the hyper parameters of the classifier. The classifier is then trained using the built-in MATLAB \textit{fitkcnn} function\cite{bib:fitkcnn}. In this case the only hyper parameter is the optimal value of k. For the ratio we've taken to split the training set, there are 1200 images for training and 300 for testing. The maximum value for k is therefore 1200 with the minimum being 1. The results for accuracy in classifying the test set of 300 images was tested for all possible values of k as illustrated in Figure \ref{fig:acc_k_1_1200}. 

\begin{figure}[h]
\centering

\begin{subfigure}{0.32\textwidth}
\centering
\includegraphics[scale=0.36]{images/acc_k_1_1200.png}
\caption{k=[1,1200]}
\label{fig:acc_k_1_1200}
\end{subfigure}
\begin{subfigure}{0.32\textwidth}
\centering
\includegraphics[scale=0.36]{images/hist_opt_k_100_runs.png}
\caption{}
\label{fig:hist_opt_k_100_runs}
\end{subfigure}
\begin{subfigure}{0.32\textwidth}
\centering
\includegraphics[scale=0.36]{images/avg_acc_k.png}
\caption{}
\label{fig:avg_acc_k}
\end{subfigure}

\caption{Getting the optimal k}
\label{fig:getting_optimal_k}
\end{figure}

\noindent
\textbf{The Optimal K :} Over a number of runs for $k=[1,1200]$, it was empirically observed that the maximum accuracy was always achieved for k values less than 30, however there was variance in the optimal k in this range. To test what value for k would generalize well, the classifier was trained over 100 runs with different randomly selected samples being used for the training and test set for cross validation. The histogram for the optimal k for every run is shown in Figure \ref{fig:hist_opt_k_100_runs}. It can be seen that $k=6$ is the mode of the distribution. However, we also wanted to know what the average accuracy was over the 100 runs for the different values of k. The average accuracy is plotted for the k values in Figure \ref{fig:avg_acc_k}. We can see that the highest average accuracy is achieved at $k=10$. However, even values for k are avoided since, in case of a tie between two class labels, either one is arbitrarily assigned. To avoid this we choose the next higher value, $k=11$.
\medskip

\noindent
\textbf{Accuracy:} The average accuracy was very low at approximately 23\%. This is primarily because the tiny feature is not very representative of the image. A lot of information in the image is thrown away when we crop and resize it. Flattening into a vector is also not very representative of the features of the image since essentially it only contains pixel brightness values. Better approaches are discussed in the next sections of this report.
\newpage

\section{Run 2, Ensemble of Classifiers with Bag-of-Visual-Words Features}

\noindent
\textbf{Feature extraction:} For this run the features considered were densely sampled 8x8 patches of the image, sampled every 4 pixels in the x and y direction. We sample 3000 patches at random from every image while also having the same number of representative images of each class for training and test (90\% and 10\% distribution). To do this, for every possible class in the labeled dataset, we randomly split it into test and training subsets and the union of all the training and test subsets is used for training or testing respectively.
\medskip

\noindent
\textbf{Learning the Vocabulary:} The vocabulary was built by clustering the patches extracted from the training images as discussed above. Initially, clustering was performed with 500 clusters using KMeans\cite{bib:kmeans} clustering but due to performance issues we decided to switch to MiniBatchKMeans\cite{bib:mini_batch_kmeans}. A batch size of 100 was used to converge the algorithm. This change significantly boosted clustering speed allowing us to test different configurations of the hyper parameters, namely the number of clusters and patches used per image. For the results submitted we used 3000 patches per image and the number of clusters was set to 700. The training time was less than half an hour on our personal computers, about 10 times less than when using regular KMeans.
\medskip


\noindent
\textbf{Bag of visual words:} To create the bag of visual words we take each image in our training set, compute it's patches and predict the cluster number for each patch. After we have this we create a histogram of the number of occurrences of every cluster number in the image. The histograms of every image are then used as 700 dimensional feature vectors to train our classifier.
\medskip

\noindent
\textbf{Training the classifiers:} Regarding the classifiers we used linear support vector classifiers as their performance was better than other classifiers considered, this decision also came from the fact that the number of features was very big when compared to the number of training examples (700 features for only 1300 training examples in the labeled set). The way we implemented this was using Sklearn's LinearSVC\cite{bib:linearSVC} in a one-versus-rest ensemble of classifiers. This trained 15 classifiers where each one specialized in detecting images from one single class.
\medskip

\begin{table}[h]
\centering
\begin{tabular}{ |c|c|c|c|c| }
 \hline
  \# of Patches & 1500 & 2000 & 2500 & 3000 \\
  \hline
  Accuracy & 44\% & 54.6\% & 49.3\% & 58.3\%\\ 
 \hline
\end{tabular}
\caption{\label{tab:patches_vs_accuracy}Number of Patches vs. Accuracy}
\end{table}

\noindent
\textbf{Accuracy:} In this run we observe a very big improvement on the accuracy of the results due to the features being much better than before. We could achieve an accuracy of up to 53-58\% on the labeled test for every run. The big variance in the accuracy results from the effect of random splits in the data and in the patches taken from the images. It should also be noted that the feature extraction method used in run 2 is not scale invariant since the size of the patch is the same for every image. This is one of the reasons why the accuracy is low. We strongly believe we can further improve the accuracy of these classifiers by extracting even better features in the next run. 
\medskip

\newpage
\section{Run 3, Ensemble of classifiers with features obtained from transfer learning}

\noindent
\textbf{Initial Approach:} The approach taken in this run was to find a good way of extracting relevant feature vectors from the images in order to train classifiers on more discriminative data. The major problem with the first two runs was that features were not very descriptive in addition to not being scale or rotation invariant. To overcome these deficiencies the first technique we employed was using ORB\cite{bib:orb_rublee} for interest point detection and feature description as an alternative to SIFT due to its open availability. This idea was abandoned as it was empirically observed that the interest points detected often missed out important regions of the picture as shown in Figure \ref{fig:orb_images}. Figure \ref{fig:orb_bed_good} illustrates a good selection, where the interest points are around the head and foot of the bed, pillows, lamp and mirror. In Figure \ref{fig:orb_bed_med} the table lamp and side table is missed while in Figure \ref{fig:orb_bed_bad} the key points are not well detected at all. We concluded that this approach was not consistent enough due to the very high variance in the interest points detected in images of the same class.

\begin{figure}[h]
\centering

\begin{subfigure}{0.32\textwidth}
\centering
\includegraphics[scale=0.31]{images/run_3/orb_bed_good1.png}
\caption{}
\label{fig:orb_bed_good}
\end{subfigure}
\begin{subfigure}{0.32\textwidth}
\centering
\includegraphics[scale=0.31]{images/run_3/orb_bed_good2.png}
\caption{}
\label{fig:orb_bed_med}
\end{subfigure}
\begin{subfigure}{0.32\textwidth}
\centering
\includegraphics[scale=0.31]{images/run_3/orb_bed_bad1.png}
\caption{}
\label{fig:orb_bed_bad}
\end{subfigure}

\caption{Interest Points detected using ORB}
\label{fig:orb_images}
\end{figure}

\noindent
\textbf{Limitations with CNNs:} Following these observations we decided to go for a Convolutional Neural Network (CNN) approach. The idea being that instead of using an ORB based approach which employs Harris corners to find interest points and descriptors around these points\cite{bib:py_opencv_orb}, we would feed the entire image to a CNN to find features descriptive of the image. The issue with this approach was that the training set contained only 1500 images which was not sufficient to adequately train the network. In view of these limitations we adopted a transfer learning based approach where we use knowledge learned on a source task to improve learning on the target task\cite{bib:transferLearning_torrey}.
\medskip

\noindent
\textbf{Transfer Learning:} It has been shown that transfer learning is effective when the target data set is significantly smaller than the source data set and when there is high correlation between the two tasks\cite{bib:NIPS2014_5347}. We have therefore focused on models trained for the ImageNet Large Scale Visual Recognition Challenge (ILSVRC) because of their ability to generalize over large numbers of testing images and classify over a large number of categories \cite{bib:huh2016makes}. Moreover, labels for objects in the ImageNet database are similar to objects in our image dataset, e.g. 'table lamp', 'pillow', 'street sign', 'traffic light', 'traffic signal', 'stoplight', etc. So we can see that there is also a high correlation between the source and target task.
\medskip

\noindent
\textbf{Pre-trained Deep Learning Models:} The models we used for transfer learning were VGG16, VGG19\cite{bib:vgg_16_19_ox} and ResNet50\cite{bib:resnet50}, available openly with their pre-trained weights on Keras\cite{bib:keras_vgg16, bib:keras_vgg19, bib:keras_resnet50}. The weights of the base models were frozen and used to generate the features. By removing the top layer of the model, the output of the 2nd last layer was flattened and used as a feature vector for every image. Depending on the architecture of the model the size of the feature vector varied.
\medskip

\noindent
\textbf{Classifier:} The remainder of the target classifier was made of a Support Vector Machine Classifier (SVC) trained with the feature vectors from the base model. Different kernels for the SVC were tried i.e. radial basis functions and polynomials of varying degree, but the best performance was observed with a linear kernel, therefore this was implemented using LinearSVC \cite{bib:linearSVC} in a one-vs-rest manner. Another approach would have been to append another neural network to the 2nd last layer of the base network and tune the weights of the network to the target task. But, for small target sets the fine tuning can result in overfitting, so we chose to freeze the weights of the base model. Additionally, as mentioned before the training set was too small to effectively train the neural network.

\begin{table}[H]
\centering
\begin{tabular}{ |c|c|c|c|c| }
 \hline
  CNN & VGG16 & VGG19 & ResNet50 \\
  \hline
  Average Accuracy & 89\% & 88\% & 94\% \\ 
 \hline
\end{tabular}
\caption{\label{tab:cnn_accuracy}Average accuracy for different pre-trained CNNs}
\end{table}

To test for generalization we randomly split the images for training and testing with a 90:10 ratio and measured accuracy across a number of runs for each of our chosen pre-trained models. The average accuracy for the models across the runs is given in Table \ref{tab:cnn_accuracy}. It can be observed that ResNet50 had the best average accuracy of all the pre-trained models at 94\%, with the lowest accuracy observed at 91\% thus making it an easy choice for the model. The class labels for the images in the coursework's test set were therefore generated using this model.

\section{Conclusions}
As shown in this report, run 3 had the best accuracy out of all, with a maximum of 94\% achieved on our labeled test data set. Despite not being able to understand the features extracted from the ResNet50 neural net we can conclude that they are much better than all the attempts tested before.
\smallskip

Given the limited number of training examples given we believe transfer learning was the most viable approach to this problem. If more training data was available a possible approach could have been to train a very specialized system for the given 15 class labels. This system could be a very deep neural network trained and fine-tuned for this classification task. Further avenues to explore could include using different classification techniques or fine-tuning the classifier's hyper-parameters trying to avoid overfitting. 

\section{Statement}
The work presented in this report and in the code was done with equal contributions from both team members. 

\newpage

\bibliographystyle{unsrt}
\bibliography{bibliography}

\end{document}
