clear all; close all; clc;

%initialize parameters for knn classification
training_ratio = 1.00;
k = 11;

disp('Reading training data...')
[trg_data, trg_labels, ~, ~] = ReadDataSet(training_ratio);

disp('Training knn model...')
Mdl = fitcknn(trg_data,trg_labels,'NumNeighbors',k);

disp('Reading test data...')
[ test_data, Files ] = ReadTestDataSet();

disp('Making predictions...')
pred = predict(Mdl, test_data);

disp('Writing results to file...')
fileID = fopen('run1.txt','w');
for k=1:length(Files)
    fprintf(fileID, [ Files(k).name ' ' strtrim(pred(k,:)) ] );
    fprintf(fileID, '\n');
end

fclose(fileID);

disp('Done!')
