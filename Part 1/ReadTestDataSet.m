function [ test_data, Files] = ReadTestDataSet()
%Function: Read 2985 images from test set
%Inputs:   None
%Output:   Double, test_data, tiny image features for all images
%          Struct, File,      struct containing filenames

%get all filenames in struct
path = '../testing/';
Files=dir([path '*.*']);
Files = Files(3:end);
test_data = zeros(2985,256);

%loop over all files
for k=1:length(Files)
   FileNames=Files(k).name;

   %read file and compute the tiny image
   img = im2double(imread([path FileNames]));
   img_feat = GetTinyImageFeature(img);
   test_data(k,:) = img_feat;
end
        
end
