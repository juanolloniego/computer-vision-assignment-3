function img_feat = GetTinyImageFeature(img)
%Function: Read 2985 images from test set
%Inputs:   Double, img,       image to extract feature from
%Output:   Double, img_feat,  tiny image features of the image

[r, c] = size(img);

% make image odd sizes to calculate center point
if ~mod(r,2)
    r = r-1;
end
if ~mod(c,2)
    c = c-1;
end

img = img(1:r,1:c);

%get center point of image
cen_r = (r+1)/2;
cen_c = (c+1)/2;

%determine cropping dimension and amount to crop
lim = min(r,c);
bound = (lim-1)/2;

%compute start and end points to crop in both dimensions
r_s = cen_r-bound;
r_e = cen_r+bound;

c_s = cen_c-bound;
c_e = cen_c+bound;

%crop the image  and resize to 16x16
img_c = img(r_s:r_e,c_s:c_e);
img_re = imresize(img_c,[16,16]);

%reshape to flatten into vector
img_feat = reshape(img_re, 1, []);
%subtract mean and normalize length
img_feat = img_feat - mean(img_feat);
img_feat = img_feat/sqrt(sum(img_feat.^2));

subplot(1,4,1); imshow(img);
subplot(1,4,2); imshow(img_re);
subplot(1,4,3); imshow(img_c);
pause;


end

