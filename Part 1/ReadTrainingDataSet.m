function [ trg_data, trg_labels, test_data, test_labels ] = ReadTrainingDataSet(training_ratio)
%Function: Read images, compute tiny feature and split into training and test set
%Inputs:   Double, training_ratio, ratio of how many images used for
%                  training
%Output:   Double, trg_data,    tiny image features for training images
%          String, trg_labels,  class labels for training images
%          Double, test_data,   tiny image features for test images
%          String, test_labels, class labels for test images

class_labels = [
    'bedroom     ';
    'Coast       '; 
    'Forest      '; 
    'Highway     '; 
    'industrial  '; 
    'Insidecity  '; 
    'kitchen     ';
    'livingroom  ';
    'Mountain    ';
    'Office      ';
    'OpenCountry ';
    'store       ';
    'Street      ';
    'Suburb      ';
    'TallBuilding'];
    
    % parameters initialisation
    numClasses = size(class_labels,1);
    numImagesInClass = 100;
    numTrainingImages = round(training_ratio*numImagesInClass);
    numTestImages = numImagesInClass-numTrainingImages;
    
    % create training image labels
    trg_data = [];
    trg_labels = reshape(repmat(class_labels(1:numClasses,:),1,numTrainingImages)',12,[])';
    
    % create test image labels
    test_data = [];
    test_labels = reshape(repmat(class_labels(1:numClasses,:),1,numTestImages)',12,[])';
    
    % loop over every class/folder
    for j = 1:numClasses
        
        % randomize selection of images chosen as training and test every
        % time called
        perm = randperm(100)-1;
        
        %loop over portion of images to get training set
        for i=1:numTrainingImages
            %read image and get its tiny feature
            img = im2double(imread(['..\training\' strtrim(class_labels(j,:)) '\' int2str(perm(i)) '.jpg']));
            img_feat = GetTinyImageFeature(img);
            trg_data = [trg_data; img_feat];
        end
        
        %loop over portion of images to get test set
        for i=(numTrainingImages+1):numImagesInClass
            %read image and get its tiny feature
            img = im2double(imread(['..\training\' strtrim(class_labels(j,:)) '\' int2str(perm(i)) '.jpg']));
            img_feat = GetTinyImageFeature(img);
            test_data = [test_data; img_feat];
        end
    end
    
end
