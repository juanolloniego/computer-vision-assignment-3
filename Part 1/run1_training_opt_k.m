clear all; close all; clc;

%initialise parameters
training_ratio = 0.80;
% max k value to test
k_max = 1;
% number of runs to do over test set
numRuns = 1;

accHist = zeros(numRuns,k_max);
opt_k = zeros(1,numRuns);
min_err = zeros(1,numRuns);
max_acc = zeros(1,numRuns);

for run=1:numRuns
    disp(['Run #' num2str(run)])
    % read data set and get training and test set
    [trg_data, trg_labels, test_data, test_labels] = ReadTrainingDataSet(training_ratio);
    
    %loop over values of k
    for k=1:k_max
        disp(['K val #' num2str(k)])
        Mdl = fitcknn(trg_data,trg_labels,'NumNeighbors',k);
        pred = predict(Mdl, test_data);

        %calculate accuracy for this k using positive classifications
        pos = 0;
        for i=1:size(test_labels,1)
            pos = pos + (strcmp(pred(i,:),test_labels(i,:)));
        end
        acc = (pos/(size(test_labels,1)))*100;
        %save accuracy for different k's for this run
        accHist(run,k) = acc;
    end

    %get optimal k for this run with highest accuracy
    [max_acc(run), opt_k(run)] = max(accHist(run,:));
    
end
