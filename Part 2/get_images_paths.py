import os
from sklearn.model_selection import train_test_split


def get_training_test_paths_labels():
    """
    Each training category gets randomly split 90-10 for test and train purposes.
    :return: The list of training and test inputs from the training dataset with their corresponding labels
    """
    # Create the lists containing the paths and labels for the images
    base_path = os.getcwd() + os.sep + ".."
    training_path = base_path + os.sep + "training"

    categories = [cat for cat in os.listdir(training_path) if not cat.startswith(".")]
    images_path_list, labels = [], []

    for category in categories:
        examples_path = training_path + os.sep + category
        count = 0
        current_paths = []
        for file in os.listdir(examples_path):
            if file.endswith(".jpg"):
                current_paths.append(examples_path + os.sep + file)
                count += 1
        images_path_list.append(current_paths)
        labels.append([category] * count)

    # Randomly split each category into training (90) and test (10) sets and flatten the lists
    training_inputs, test_inputs, training_labels, test_labels = [], [], [], []
    for idx, element in enumerate(images_path_list):
        train_x, test_x, train_y, test_y = train_test_split(element, labels[idx], test_size=0.1)
        training_inputs.extend(train_x)
        test_inputs.extend(test_x)
        training_labels.extend(train_y)
        test_labels.extend(test_y)

    return training_inputs, test_inputs, training_labels, test_labels


def get_testing_paths():
    """
    :return: The real test dataset (the one for which we need to write predictions in a file) paths.
    """
    base_path = os.getcwd() + os.sep + ".."
    test_path = base_path + os.sep + "testing"

    test_image_paths = []
    for file in os.listdir(test_path):
        if file.endswith(".jpg"):
            test_image_paths.append(test_path + os.sep + file)

    return test_image_paths
