import numpy as np
from sklearn import preprocessing


def extract_patches(image_matrix, patch_size, step_length, sample):
    """
    :param image_matrix: The image to get the patches from
    :param patch_size: the N dimension for NxN patches
    :param step_length: how much to move in the x and y direction before taking a new patch
    :param sample: the amount of patches to randomly sample from all the possible ones
    :return: a list containing the sampled patches
    """
    patches = []
    height, width = image_matrix.shape
    for i in range(0, width, step_length):
        i_end = i + patch_size
        for j in range(0, height, step_length):
            j_end = j + patch_size
            if i_end < width and j_end < height:
                flattened_patch = np.asarray(image_matrix[j:j_end, i:i_end]).reshape((1, patch_size * patch_size)).astype(np.float64)
                flattened_patch -= flattened_patch.mean()
                patches.extend(preprocessing.normalize(flattened_patch, norm='l2'))

    patches = np.asarray(patches)
    # Sample 'sample_size' random patches
    if sample != 0:
        indexes = np.random.randint(0, len(patches), size=sample)
        patches = patches[indexes]

    return patches
