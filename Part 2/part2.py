import cv2
import time
from sklearn.svm import LinearSVC
from clustering_model import train_or_load_model
from extract_image_features import extract_patches
from get_images_paths import get_training_test_paths_labels, get_testing_paths


def generate_images_histograms(image_paths, clustering_model, histogram_length, p_size, step, sample):
    # Take each training image and represent it as a histogram of its words
    result = []
    for image_path in image_paths:
        image_matrix = cv2.imread(image_path, 0)
        image_histogram = [0] * histogram_length
        image_patches = extract_patches(image_matrix, patch_size=p_size, step_length=step, sample=sample)
        # Get the clusters to which each image patch belongs and add it to the histogram
        clusters = clustering_model.predict(image_patches)
        for cluster in clusters:
            image_histogram[cluster] += 1
        result.append(image_histogram)
    return result


training_paths, test_paths, training_labels, test_labels = get_training_test_paths_labels()

sample_size = 3000  # Number of patches to sample from the image
step_length = 4
patch_size = 8
N_clusters = 700
vocabulary_model = train_or_load_model(training_paths, N_clusters, patch_size, step_length, sample_size)

print("Generating training data histograms")
start = time.time()
training_data = generate_images_histograms(training_paths, vocabulary_model, N_clusters, patch_size, step_length, sample_size)

histograms, corresponding_labels = zip(*sorted(zip(training_data, training_labels), key=lambda x: x[1]))
final = time.time()
print("Training data done: ", final - start)


# trains n_classes one-vs-rest svc classifiers
print("Training classifiers")
start = time.time()
classifier = LinearSVC(multi_class='ovr', max_iter=20000)
classifier.fit(histograms, corresponding_labels)
final = time.time()
print("Classifiers done: ", final - start)

# Measure the accuracy on the test data
test_data = generate_images_histograms(test_paths, vocabulary_model, N_clusters, patch_size, step_length, sample_size)

# Count the true positives in the predictions
print("Predictions for the test set of the training data")
predictions = classifier.predict(test_data)
correct = 0
for idx, label in enumerate(test_labels):
    if label == predictions[idx]:
        correct += 1

print(correct, " out of ", len(test_labels))
print(correct/len(test_labels)*100)

# Predict the labels of the real test images and output a file result

real_test_paths = get_testing_paths()
test_histograms = generate_images_histograms(real_test_paths, vocabulary_model, N_clusters, patch_size, step_length, sample_size)
predictions = classifier.predict(test_histograms)

with open("run2.txt", 'w') as out_file:
    for i in range(0, len(predictions)):
        image_name = real_test_paths[i].split("/")[-1]
        out_file.write("{} {}\n".format(image_name, predictions[i]))


