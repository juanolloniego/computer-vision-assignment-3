import os
import cv2
import time
import numpy as np
from joblib import dump, load
from sklearn.cluster import MiniBatchKMeans
from extract_image_features import extract_patches

MODEL_PATH = "models" + os.sep + "kmeans.joblib"


def train_or_load_model(training_paths, k_clusters, patch_size, step_length, sample_size):
    """
    Returns a clustering model by either loading it from a file or training a new one.
    :param training_paths: the paths of all training examples to cluster from.
    :param k_clusters: the number of clusters to use
    :param patch_size: the N dimension for the NxN patches
    :param step_length: how much to move along the x and y direction before taking a new patch
    :param sample_size: how many patches to randomly sample from all the possible ones
    :return: the sklearn model.
    """
    if os.path.isfile(MODEL_PATH):
        print("Loaded Model")
        return load(MODEL_PATH)
    else:
        # No model file to load, so train and save a new one
        all_patches = []
        # Load each image and compute its features
        for idx, image_path in enumerate(training_paths):
            image_matrix = cv2.imread(image_path, 0)
            # Compute the features
            current_patches = extract_patches(image_matrix, patch_size=patch_size, step_length=step_length, sample=sample_size)
            all_patches.extend(current_patches)

        start = time.time()
        learned_model = MiniBatchKMeans(n_clusters=k_clusters, init_size=k_clusters*3, max_iter=k_clusters)
        learned_model.fit(np.asarray(all_patches))
        finish = time.time()
        print("Training time: ", finish - start)
        dump(learned_model, MODEL_PATH)
        return learned_model
